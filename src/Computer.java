public class Computer implements Actions{
    public void sleep() {
        System.out.println("Windows is hibernating");
    }

    public void run(){
        System.out.println("Windows is running.");
    }

    //Since a polymorphed run method with overloaded arguments are added in the interface, Actions,
    //Computer class should have a run() method with the same dataType arguments.
}
