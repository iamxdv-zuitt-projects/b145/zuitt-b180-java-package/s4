public class Person implements Actions,Greetings {
    //To be able for your class to subscribe to an interface, it should be able to "implement" that interface.
    public void sleep(){
        System.out.println("Zzzzz...");
    }

    public void run() {
        System.out.println("Running on the road!");
    }
    public void run(String name, int speed) {
        System.out.println(name + " is running at: " + speed + "kph");
    }

    public void morningGreet(){
        System.out.println("Good Morning, Friend!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays, Friend!");
    }
}
