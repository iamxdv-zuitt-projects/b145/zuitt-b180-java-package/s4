public interface Actions {

    //Interface are used to achieve abstraction. Users of our app will only be able to use the methods but not be able to see how those methods work.
    //Classes cannot have multiple inheritance.
    //Classes can refer/implement multiple interfaces. You can think of interfaces as "class" for classes or blueprints for blueprint
    public void sleep();
    public void run();
    //Static Polymorphism in interface: Only the method and arguments but no definition of method.
    public void run(String name,int speed);

}
