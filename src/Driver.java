public class Driver {
    /*
        Activity 1:

        Create a class called Driver with the following attributes:
        name - string
        age - int
        address - string


        Add a default and parameterized constructor for the class.
        Add getters and setters for the name,age and address attributes.
        In the main method of the Main class, create a new instance of the Driver class and store it in a variable called driver1.
            - initialize values for the new driver object.
        -In your zuitt-b180 folder, Create a git repository named WDC043-S04.
        -Initialize a local git repository in your s4 folder, add the remote link and push to git with the commit message of Add activity 1 code.
        -Link your online repository in boodle.

    */
    /*
        Classes have relationships:

        A Car could have a Driver. - "has relationship" - Composition - allows modelling objects to be made up of other objects.

    */
    private String name;
    private int age;
    private String address;

    public Driver() {
    }

    public Driver(String name, int age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
